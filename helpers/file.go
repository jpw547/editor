package helpers

import (
	"encoding/json"
	"fmt"
	"harward-media/editor/structs"
	"io/ioutil"
)

// WriteDataToFile writes the editor data to a file
func WriteDataToFile(data structs.EditorData) error {
	file, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		fmt.Printf("failed to marshal data: %s", err.Error())
		return err
	}

	err = ioutil.WriteFile("last_editor.json", file, 0644)
	if err != nil {
		fmt.Printf("failed to write data to file: %s", err.Error())
		return err
	}

	return nil
}

// ReadDataFromFile reads the editor data from a file
func ReadDataFromFile() (structs.EditorData, error) {
	var data structs.EditorData

	b, err := ioutil.ReadFile("last_editor.json")
	if err != nil {
		fmt.Printf("failed to read data from file: %s", err.Error())
		return structs.EditorData{}, err
	}

	err = json.Unmarshal(b, &data)
	if err != nil {
		fmt.Printf("failed to unmarshal data: %s", err.Error())
		return structs.EditorData{}, err
	}

	return data, nil
}
