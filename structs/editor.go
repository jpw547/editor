package structs

// EditorData is the format that the editor returns data in
type EditorData struct {
	Time    int     `json:"time"`
	Blocks  []Block `json:"blocks"`
	Version string  `json:"version"`
}

// Block is a piece of the editor data
type Block struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}
