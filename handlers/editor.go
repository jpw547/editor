package handlers

import (
	"fmt"
	"harward-media/editor/helpers"
	"harward-media/editor/structs"
	"net/http"

	"github.com/labstack/echo"
)

// SaveEditorData binds incoming editor data and saves it...somewhere
func SaveEditorData(ctx echo.Context) error {
	// fmt.Print("trying to save some data\n")
	var data structs.EditorData

	err := ctx.Bind(&data)
	if err != nil {
		fmt.Printf("failed to bind request data: %s", err.Error())
		return ctx.JSON(http.StatusBadRequest, err)
	}

	err = helpers.WriteDataToFile(data)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	return ctx.JSON(http.StatusOK, "ok")
}

// GetEditorData gets the saved editor data and returns it
func GetEditorData(ctx echo.Context) error {
	// fmt.Print("trying to read some data\n")
	data, err := helpers.ReadDataFromFile()
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	return ctx.JSON(http.StatusOK, data)
}
