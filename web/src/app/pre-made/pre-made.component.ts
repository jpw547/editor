import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import EditorJS from '@editorjs/editorjs';
import ImageTool from '@editorjs/image';

@Component({
  selector: 'app-pre-made',
  templateUrl: './pre-made.component.html',
  styleUrls: ['./pre-made.component.scss']
})
export class PreMadeComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
    const editor = new EditorJS({
      tools: {
        image: {
          class: ImageTool,
          config: {}
        }
      }
    })
  }

  switch() {
    this.router.navigate([""]);
  }
}
