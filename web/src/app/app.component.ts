import { Component } from '@angular/core';
import EditorJS, { API, ToolSettings, BaseTool } from '@editorjs/editorjs';
import { Image } from './tools/image';
import { Quote } from './tools/quote';
import { Video } from './tools/video';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'web';

  constructor(public router: Router) {
  }

}
