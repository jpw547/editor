export class Image {
    data;
    api;
    config;
    wrapper;
    settings;

    static get toolbox() {
        const i = require('!!raw-loader?!../../assets/insert_photo-24px.svg');
        return {
            title: 'Image',
            icon: i.default
        }; 
    }

    static get pasteConfig() {
        return {
            tags: ['IMG'],
            files: {
                mimeTypes: ['image/*'],
                extensions: ['gif', 'jpg', 'png']
            },
            patterns: {
                image: /https?:\/\/\S+\.(gif|jpe?g|tiff|png)$/i
            }
        }
    }

    static get sanitize() {
        return {
            url: false,
            caption: {}
        }
    }

    constructor({data, api, config}) {
        this.api = api;
        this.config = config || {};
        this.data = {
            url: data.url || '',
            caption: data.caption || '',
            withBorder: data.withBorder !== undefined ? data.withBorder : false,
            withBackground: data.withBackground !== undefined ? data.withBackground : false,
            stretched: data.stretched !== undefined ? data.stretched : false,
        };

        this.wrapper = undefined;
        
        const border = require('!!raw-loader?!../../assets/border_outer-24px.svg');
        const background = require('!!raw-loader?!../../assets/wallpaper-24px.svg');
        const stretch = require('!!raw-loader?!../../assets/panorama_horizontal-24px.svg');
        this.settings = [
            {
                name: 'withBorder',
                icon: border.default,
            },
            {
                name: 'stretched',
                icon: stretch.default
            },
            {
                name: 'withBackground',
                icon: background.default
            }
        ];
    }

    // API Methods

    render() {
        this.wrapper = document.createElement('div');
        const input = document.createElement('input');
    
        this.wrapper.classList.add('simple-image');
    
        if (this.data && this.data.url){
            this._createImage(this.data.url, this.data.caption);
            return this.wrapper;
        }
    
        this.wrapper.appendChild(input);
    
        input.placeholder = this.config.placeholder || 'Paste an image URL...';
        input.value = this.data && this.data.url ? this.data.url : '';
    
        input.addEventListener('paste', (event) => {
            this._createImage(event.clipboardData.getData('text'));
        });
    
        return this.wrapper;
    }

    save(blockContent) {
        const image = blockContent.querySelector('img');
        const caption = blockContent.querySelector('[contenteditable]');
        const sanitizerConfig = {
            b: true,
            a: {
                href: true
            },
            i: true
        };
    
        return Object.assign(this.data, {
            url: image.src,
            caption: caption.innerHTML || ''
        });
    }

    validate(savedData) {
        if (!savedData.url.trim()) {
            return false;
        }

        return true;
    }
    
    renderSettings() {
        const wrapper = document.createElement('div');
    
        this.settings.forEach( tune => {
            let button = document.createElement('div');
    
            button.classList.add(this.api.styles.settingsButton, this.data[tune.name]);
            button.classList.toggle(this.api.styles.settingsButtonActive, this.data[tune.name]);
            button.innerHTML = tune.icon;
            wrapper.appendChild(button);
    
            button.addEventListener('click', () => {
                this._toggleTune(tune.name);
                button.classList.toggle(this.api.styles.settingsButtonActive);
            });
        });
    
        return wrapper;
    }

    onPaste(event) {
        switch (event.type) {
            case 'tag':
                const imgTag = event.detail.data;

                this._createImage(imgTag.src);
                break;
            case 'file':
                const file = event.detail.file;
                const reader = new FileReader();

                reader.onload = (loadEvent) => {
                    this._createImage(loadEvent.target.result);
                };

                reader.readAsDataURL(file);
                break;
            case 'pattern':
                const src = event.detail.data;

                this._createImage(src);
                break;
        }
    }

    // Private Methods

    private _createImage(url, captionText?) {
        const image = document.createElement('img');
        const caption = document.createElement('div');

        image.src = url;
        caption.contentEditable = 'true';
        caption.innerHTML = captionText || '';

        this.wrapper.innerHTML = '';
        this.wrapper.appendChild(image);
        this.wrapper.appendChild(caption);

        this._acceptTuneView();
    }

    private _toggleTune(tune) {
        this.data[tune] = !this.data[tune];
        this._acceptTuneView();
    }

    private _acceptTuneView() {
        this.settings.forEach(tune => {
            this.wrapper.classList.toggle(tune.name, !!this.data[tune.name]);

            if (tune.name === 'stretched') {
                this.api.blocks.stretchBlock(this.api.blocks.getCurrentBlockIndex(), !!this.data.stretched);
            }
        })
    }
}