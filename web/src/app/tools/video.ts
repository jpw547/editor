import { DomSanitizer } from '@angular/platform-browser';

export class Video {
    data;
    api;
    config;
    wrapper;
    settings;

    static get toolbox() {
        const i = require('!!raw-loader?!../../assets/videocam-24px.svg');
        return {
            title: 'Video',
            icon: i.default
        };
    }

    static get pasteConfig() {
        return {}
    }

    constructor({data, api, config}) {
        this.api = api;
        this.config = config || {};
        this.data = {
            url: data.url || '',
            caption: data.caption || '',
            withBorder: data.withBorder !== undefined ? data.withBorder : false,
            withBackground: data.withBackground !== undefined ? data.withBackground : false,
            stretched: data.stretched !== undefined ? data.stretched : false,
        };

        console.log(this.config);

        this.wrapper = undefined;
        
        const border = require('!!raw-loader?!../../assets/border_outer-24px.svg');
        const background = require('!!raw-loader?!../../assets/wallpaper-24px.svg');
        const stretch = require('!!raw-loader?!../../assets/panorama_horizontal-24px.svg');
        this.settings = [
            {
                name: 'withBorder',
                icon: border.default,
            },
            {
                name: 'stretched',
                icon: stretch.default
            },
            {
                name: 'withBackground',
                icon: background.default
            }
        ];
    }

    render() {
        this.wrapper = document.createElement('div');
        const input = document.createElement('input');
    
        this.wrapper.classList.add('simple-image');
    
        if (this.data && this.data.url){
            this._createVideo(this.data.url, this.data.caption);
            return this.wrapper;
        }
    
        this.wrapper.appendChild(input);
    
        input.placeholder = this.config.placeholder || 'Paste an image URL...';
        input.value = this.data && this.data.url ? this.data.url : '';
    
        input.addEventListener('paste', (event) => {
            this._createVideo(event.clipboardData.getData('text'));
        });
    
        return this.wrapper;
    }

    save(blockContent) {
        const image = blockContent.querySelector('iframe');
        const caption = blockContent.querySelector('[contenteditable]');
        const sanitizerConfig = {
            b: true,
            a: {
                href: true
            },
            i: true
        };
    
        return Object.assign(this.data, {
            url: image.src,
            caption: caption.innerHTML || ''
        });
    }


    // Private Methods //

    private _createVideo(url, captionText?) {
        const iframe = document.createElement('iframe');
        const caption = document.createElement('div');

        const sani = this.config.urlSanitizer as DomSanitizer;
        const safeURL = sani.bypassSecurityTrustResourceUrl(url);

        iframe.src = safeURL as string;
        iframe.frameBorder = '0';
        iframe.allowFullscreen = true;

        caption.contentEditable = 'true';
        caption.innerHTML = captionText || '';

        this.wrapper.innerHTML = '';
        this.wrapper.appendChild(iframe);
        this.wrapper.appendChild(caption);
    }
}