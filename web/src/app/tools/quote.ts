export class Quote {
    data;
    api;
    config;
    wrapper;
    settings;

    bodyID = 'quoteBody';
    authID = 'quoteAuth';

    static get toolbox() {
        const i = require('!!raw-loader?!../../assets/format_quote-24px.svg');
        return {
            title: 'Quote',
            icon: i.default
        };
    }

    static get pasteConfig() {
        return {

        }
    }

    constructor({data, api, config}) {
        this.api = api;
        this.config = config || {};
        this.data = {
            quote: data.quote || '',
            author: data.author || '',
            withBorder: data.withBorder !== undefined ? data.withBorder : false,
            withBackground: data.withBackground !== undefined ? data.withBackground : false,
            stretched: data.stretched !== undefined ? data.stretched : false,
        };

        this.wrapper = undefined;
        
        const border = require('!!raw-loader?!../../assets/border_outer-24px.svg');
        const background = require('!!raw-loader?!../../assets/wallpaper-24px.svg');
        const stretch = require('!!raw-loader?!../../assets/panorama_horizontal-24px.svg');
        this.settings = [
            {
                name: 'withBorder',
                icon: border.default
            },
            {
                name: 'stretched',
                icon: stretch.default
            },
            {
                name: 'withBackground',
                icon: background.default
            }
        ];
    }

    render() {
        this.wrapper = document.createElement('div');
        const body = document.createElement('textarea');
        const auth = document.createElement('input');

        this.wrapper.classList.add('quote-box');

        body.id = this.bodyID;
        auth.id = this.authID;

        body.classList.add('quote-input', 'large');
        auth.classList.add('quote-input');

        body.contentEditable = 'true';
        auth.contentEditable = 'true';

        body.placeholder = this.config.placeholder || 'Insert quote here...';
        body.value = this.data && this.data.qoute ? this.data.quote : '';

        auth.placeholder = 'Put author here...';
        auth.value = this.data && this.data.author ? this.data.author : '';

        this.wrapper.appendChild(body);
        this.wrapper.appendChild(auth);
        return this.wrapper;
    }

    save(blockContent) {
        const body = blockContent.querySelector('#' + this.bodyID);
        const auth = blockContent.querySelector('#' + this.authID);

        return Object.assign(this.data, {
            quote: body.value || '',
            author: auth.value || ''
        });
    }

    renderSettings() {

    }
}