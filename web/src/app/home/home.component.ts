import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import EditorJS, { API, ToolSettings, BaseTool, OutputData } from '@editorjs/editorjs';
import { Image } from '../tools/image';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import Embed from '@editorjs/embed';
import Header from '@editorjs/header';
import Checklist from '@editorjs/checklist';
import Quote from '@editorjs/quote';
import List from '@editorjs/list';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  editor: EditorJS;
  editorData: any;

  constructor(private _sanitizer: DomSanitizer, public router: Router, private http: HttpClient) {
  }

  ngOnInit() {
    this.initEditor();  
  
    const saveButton = document.getElementById('save-button');
    const output = document.getElementById('output');
  
    saveButton.addEventListener('click', () => {
      this.editor.save().then(savedData => {
        output.innerHTML = JSON.stringify(savedData, null, 4);
        output.classList.add('visible');
        this.saveToBackEnd(savedData);
      })
    })
  }

  initEditor() {
    this.editor = new EditorJS({
      data: this.editorData !== undefined ? this.editorData : {},
      autofocus: true,
      tools: {
        image: {
          class: Image,
          inlineToolbar: true,
          config: {
            placeholder: 'Paste image URL'
          }
        },
        quote: {
          class: Quote,
          inlineToolbar: true,
          shortcut: 'CMD+SHIFT+O',
          config: {
            quotePlaceholder: 'Enter a quote',
            captionPlaceholder: 'Quote\'s author',
          },
        },
        embed: {
          class: Embed,
          inlineToolbar: true,
        },
        header: {
          class: Header,
          shortcut: 'CMD+SHIFT+H',
          config: {
            placeholder: 'Enter a header',
            levels: [2, 3, 4],
            defaultLevel: 3
          }
        },
        checklist: {
          class: Checklist,
          inlineToolbar: true,
        },
        list: {
          class: List,
          inlineToolbar: true,
        },
      }
    });
  }

  switch() {
    this.router.navigate(["/plugin"]);
  }

  async saveToBackEnd(data) {
    try {
      console.log("saving to back end...")

      const resp = await this.http.put("http://localhost:1600/data", data).toPromise();

      console.log("here is the response from saving: ", resp);
    } catch (e) {
      throw new Error("error saving to the back end: " + e);
    }
  }

  async getDataFromBackEnd() {
    try {
      console.log("getting data from back end...")

      const resp = await this.http.get("http://localhost:1600/data").toPromise();
      console.log("here is the response from getting", resp);
      this.editor.render(resp as OutputData);
    } catch (e) {
      throw new Error("error getting data from the back end: " + e);
    }
  }
}
