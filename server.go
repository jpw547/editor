package main

import (
	"errors"
	"harward-media/editor/handlers"
	"log"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()

	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.CORS())

	e.PUT("/data", handlers.SaveEditorData)
	e.GET("/data", handlers.GetEditorData)

	e.Group("", middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   "web-dist",
		Index:  "index.html",
		HTML5:  true,
		Browse: true,
	}))

	err := e.Start(":1600")
	if err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatal("failed to start server", err.Error())
	}
}
