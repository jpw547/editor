# go
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get

# angular
NPM=npm
NPM_INSTALL=$(NPM) install
NG_BUILD=ng build --prod --aot --build-optimizer 
NG1=web

all: build

build: build-web

build-web: $(NG1)
	cd $(NG1) && $(NPM_INSTALL) && $(NG_BUILD) --base-href="/"
	rm -rf $(NG1)-dist
	mv -f $(NG1)/dist/$(NG1) $(NG1)-dist

clean: 
	$(GOCLEAN)
	# rm -f $(NAME)-bin
	# rm -f $(NAME)-arm
	rm -rf $(NG1)-dist
	# rm -rf vendor/

$(NG1)-dist:
	$(MAKE) build-web